# Whirlwind CI/CD

A .gitlab-ci.yml file that will be pulled into your Whirlwind project as a starting point for CI/CD.

## Gitlab configuration

Project variables apply only to that project. They can be accessed by going
to the project and navigating to
Settings --> CI/CD --> Environment variables.

## Global project variables

### `PLATFORM` variable

The hosting platform that you will be using.  Valid values include '**pantheon**' or '**acquia**'.

### `SSH_PRIVATE_KEY` variable

Paste the private key for the CI user account that will be accessing the Dev and
Staging servers.  This, for the time being, should be added by Steve at time of setup.

### `DEV_SITE_URL` variable

The URL to the development environment on either Pantheon or Acquia.  This is used for DAST testing after deployment.

## Platform project variables

### Pantheon

#### `PANTHEON_GIT_URL` variable

Copy **Clone with Git** url from Pantheon environment, starting with ssh and ending with .git.

Example:
`ssh://codeserver.dev.ca9b3b7e-9ad6-417f-9e74-231aaf9462a2@codeserver.dev.ca9b3b7e-9ad6-417f-9e74-231aaf9462a2.drush.in:2222/~/repository.git`

#### `PANTHEON_SITE_ID` variable

Needed for running the scheduled `backup_pantheon` Gitlab job. Can be found
on the site Connection Info drop down as the last part of the **SSH Clone URL** string.

Example:
`git clone ssh://codeserver.dev.97f7ddc4-4723-47b4-9198-2724fb0b420c@codeserver.dev.97f7ddc4-4723-47b4-9198-2724fb0b420c.drush.in:2222/~/repository.git site-id-here`

### Acquia

#### `ACQUIA_GIT_URL` variable

Copy **Git Info -> URL** from Acquia environment and include ssh://.

Example:
`ssh://irvingoil@svn-29892.prod.hosting.acquia.com:irvingoil.git`



